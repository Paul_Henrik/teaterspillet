﻿using UnityEngine;
using System.Collections;

public class PlayerAudioScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void PlaySound (string soundRoot, string soundObject, bool play)
	{
		GameObject soundParent;
		GameObject soundRootObject;
		GameObject sound;
		soundParent = GameObject.FindGameObjectWithTag ("sfxAudio");

		if (play) {
			if (soundRoot == "Player") {
				soundRootObject = soundParent.transform.FindChild ("Player").gameObject;
				if (soundObject == "Attack") {
//					GameObject[] Sounds = null;
//					int index;
//					Sounds [0] = soundRootObject.transform.FindChild ("AttackOne").gameObject;
//					Sounds [1] = soundRootObject.transform.FindChild ("AttackTwo").gameObject;
//					Sounds [3] = soundRootObject.transform.FindChild ("AttackThree").gameObject;
//					index = Random.Range (0, Sounds.Length);
//					sound = Sounds [index];
					sound = soundRootObject.transform.FindChild ("AttackOne").gameObject;
					sound.GetComponent<AudioSource> ().Play ();
					Debug.Log ("Attack sound");
				}
				if (soundObject == "Balade") {
					sound = soundRootObject.transform.FindChild ("Balade").gameObject;
					sound.GetComponent<AudioSource> ().Play ();
					Debug.Log ("Gentle Lute");
                
				}
                if (soundObject == "PlayLute")
                {
                    sound = soundRootObject.transform.FindChild("PlayLute").gameObject;
                    sound.GetComponent<AudioSource>().Play();
                }

                if (soundObject == "Hurt") {
//					GameObject[] Sounds = null;
//					int index;
//					Sounds [0] = soundRootObject.transform.FindChild ("HurtOne").gameObject;
//					Sounds [1] = soundRootObject.transform.FindChild ("HurtTwo").gameObject;
//					Sounds [3] = soundRootObject.transform.FindChild ("HurtThree").gameObject;
//					index = Random.Range (0, Sounds.Length);
//					sound = Sounds [index];
					sound = soundRootObject.transform.FindChild ("HurtOne").gameObject;
					sound.GetComponent<AudioSource> ().Play ();
					Debug.Log ("hurt");
				}
				if (soundObject == "MetalLute") {
					sound = soundRootObject.transform.FindChild ("MetalLute").gameObject;
					sound.GetComponent<AudioSource> ().Play ();
					Debug.Log ("Harsh Lute");
				}
				if (soundObject == "SwordAttack") {
					sound = soundRootObject.transform.FindChild ("SwordAttack").gameObject;
					sound.GetComponent<AudioSource> ().Play ();
					Debug.Log ("Sword sound");
				}
				if (soundObject == "Tada") {
//					GameObject[] Sounds = null;
//					int index;
//					Sounds [0] = soundRootObject.transform.FindChild ("TadaOne").gameObject;
//					Sounds [1] = soundRootObject.transform.FindChild ("TadaTwo").gameObject;
//					Sounds [3] = soundRootObject.transform.FindChild ("TadaThree").gameObject;
//					index = Random.Range (0, Sounds.Length);
//					sound = Sounds [index];
					sound = soundRootObject.transform.FindChild ("TadaOne").gameObject;
					sound.GetComponent<AudioSource> ().Play ();
					Debug.Log ("Tada");
				}
				if (soundObject == "Triumph") {
//					GameObject[] Sounds = null;
//					int index;
//					Sounds [0] = soundRootObject.transform.FindChild ("TriumphOne").gameObject;
//					Sounds [1] = soundRootObject.transform.FindChild ("TriumphTwo").gameObject;
//					Sounds [3] = soundRootObject.transform.FindChild ("TriumphThree").gameObject;
//					index = Random.Range (0, Sounds.Length);
//					sound = Sounds [index];
					sound = soundRootObject.transform.FindChild ("TriumphOne").gameObject;
					sound.GetComponent<AudioSource> ().Play ();
					Debug.Log ("Triumph");
				}
			}





			play = false;
		}
	}
}
