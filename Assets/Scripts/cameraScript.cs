﻿using UnityEngine;
using System.Collections;

public class cameraScript : MonoBehaviour
{
    public Transform target;         //Cameras target;
    private Vector3 startPoint;      //saves startPoint;
    public float zoomSpeed;          //How fast the camera zoomes
    public float maxZoom = 4;
    public Camera mainCam;


    void Start()
    {
        
        startPoint = transform.position;
        target = GetComponentInParent<CameraTarget>().target.transform;            //Gets target from the parent.
        mainCam = GetComponent<Camera>();
    }

	void Update ()
	{
        if (target == null)
        {
            target = GetComponentInParent<CameraTarget>().target.transform;
        }

        RaycastHit hit;                                                                                     //What the ray hits
        Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);                                            //Gets the mouse position

//        float scroll = Input.GetAxis("Mouse ScrollWheel");
//        if (scroll > 0f)
//        {
////            Debug.Log("Scrolling inn");
//        }
//
//        if ((scroll > 0f) && transform.position.z < startPoint.z + maxZoom)
//            {
//                float step = zoomSpeed * Time.deltaTime;
//                transform.position = Vector3.MoveTowards(transform.position, target.position, step);
//                //transform.position += new Vector3(0f, 0f, zoomSpeed);
//            }
//            if ((scroll < 0f) && transform.position.z > startPoint.z)
//            {
//                float step = zoomSpeed * Time.deltaTime;
//                transform.position = Vector3.MoveTowards(transform.position, target.position, -step);
//            }

        if (Physics.Raycast(ray, out hit))
        {
            Transform objectHit = hit.transform;
//            Debug.Log(objectHit);
            Vector3 rayDir = transform.position - objectHit.transform.position;                             //Raycast direction, for debugging
//            Debug.DrawRay(transform.position, rayDir * -100, Color.green);                                  //draws the raycast


        }


    }
}