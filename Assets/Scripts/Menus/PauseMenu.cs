﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    public string ResetLevel;

    public bool isPaused = false;
    public bool isSettings = false;
	public bool mainMenu;
	public float pauseDelay = 1f;
	private GameObject cameratarget;

    public GameObject pauseMenuCanvas;
    public GameObject settingsCanvas;

	void Start()
	{
		cameratarget = GameObject.Find ("Camera");
	}

    // Update is called once per frame
    void Update ()
    {
		
        if (isPaused)
        {
			StartCoroutine (pauseTime ());
        }
        else if (!isPaused)
        {
            pauseMenuCanvas.SetActive(false);
            settingsCanvas.SetActive(false);
			Time.timeScale = 1f;
			cameratarget.GetComponent<CameraTarget> ().LookAtMenu (false);
        }

        if(isPaused && isSettings)
        {
            pauseMenuCanvas.SetActive(false);
            settingsCanvas.SetActive(true);
        }
        else if (isPaused && !isSettings)
        {
            pauseMenuCanvas.SetActive(true);
            settingsCanvas.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Escape) && !isSettings)
        {
			cameratarget.GetComponent<CameraTarget> ().LookAtMenu (true);
            isPaused = !isPaused;
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && isSettings)
        {
            isSettings = false;
        }
    }


    public void ResumeGame()
    {
        isPaused = false;
    }

    public void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Settings()
    {
        isSettings = true;
    }

    public void ExitMainMenu()
    {
        SceneManager.LoadScene(ResetLevel);
    }

    public void ExitDesktop()
    {
        Application.Quit();
    }

	IEnumerator pauseTime()
	{
		yield return new WaitForSeconds (pauseDelay);
		Time.timeScale = 0f;
	}

	IEnumerator tiltUp(){
		yield return new WaitForSeconds (pauseDelay);
		cameratarget.GetComponent<CameraTarget> ().LookAtMenu (false);
	}
    
}


/*
    I editor:
    Main menu stringen må hete NØYAKTIG det main menu scenen heter!! Veldig viktig!!!!!!!
    PauseMenuCanvas bør ligge inne, hvis ikke er det pause meny canvaset
    SettingsCanvas er settings canvaset som du henter fra Menu Setup prefaben.
*/
