﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ResetGameScript : MonoBehaviour {

    Scene scene;

	// Update is called once per frame
	void Update ()
    {
        scene = SceneManager.GetActiveScene();

	    if (scene.name == "ResetLevel")
        {
            Destroy(gameObject);
        }
	}
}
