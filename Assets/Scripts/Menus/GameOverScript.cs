﻿using UnityEngine;
using System.Collections;

public class GameOverScript : MonoBehaviour
{
    public GameObject gameOverCanvas;
    public AudienceAI audience;
    public string ResetLevel;


	// Use this for initialization
	void Start ()
    {
        ResetLevel = "ResetLevel";
        gameOverCanvas.SetActive(false);
        audience = GameObject.FindGameObjectWithTag("Audience").GetComponent<AudienceAI>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (audience.GameOver)
        {
            gameOverCanvas.SetActive(true);
            Time.timeScale = 0f;
            Debug.Log("gameover active");
        }

        if (audience.GameOver && Input.anyKey)
        {
            Application.LoadLevel(ResetLevel);
            Debug.Log("Moving to reset");
        }

	
	}

}
