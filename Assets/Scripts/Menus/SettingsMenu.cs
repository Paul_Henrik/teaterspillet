﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SettingsMenu : MonoBehaviour {
    public AudioSource music;
    public AudioSource sfx;
    public AudioSource[] audience;

	public GameObject AudienceGameobject;
    public GameObject pauseMenuCanvas;
    public GameObject settingsCanvas;
	public GameObject mainMenuCanvas;
	public bool mainMenu;

    

	public void MasterVolume(float Volume)
    {
        AudioListener.volume = Volume;
    }

    public void MusicVolume(float Volume)
    {
        music.volume = Volume;
    }

    public void AudienceVolume(float Volume)
    {
		for(int i = 0; i < audience.Length; i++)
        audience[i].volume = Volume;

    }

    public void SfxVolume(float Volume)
    {
        sfx.volume = Volume;
    }

    void Awake()
    {
        if (SceneManager.GetActiveScene().name != "MainMenu")
        audience = GameObject.FindGameObjectWithTag("Audience").GetComponentsInChildren<AudioSource>();
    }

    void Start()
    {
  //      settingsCanvas.SetActive(false);
    }

	void Update()
    {
		if (Input.GetKey (KeyCode.Escape) && mainMenu)
        {
			settingsCanvas.SetActive (false);
			mainMenuCanvas.SetActive (true);
		}
	}
}

/*
    Resolution og credits har jeg ikke laget enda. De står tomme intil videre. Master volume trenger ikke noe input, ettersom den plukker opp all
    lyden i en scene.
    Må kanskje lage arrays av AudioSources til de andre sliderene?!?!? Må i alle fall poengtere hvilke audioSource scriptet skal hente inn.
*/