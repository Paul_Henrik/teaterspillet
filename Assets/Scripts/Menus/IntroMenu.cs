﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class IntroMenu : MonoBehaviour {

    //For rotating camera
    public bool anythingPressed;
	public bool mainMenu; 
    public GameObject camera;
    public float angleRotated = 33;
    

    //For buttons
    public string startLevel;
    public GameObject mainMenuButtonCanvas;
    public GameObject settingsCanvas;

    void Start ()
    {
        anythingPressed = false;
		settingsCanvas.SetActive (false);
	}
	
	void Update ()
    {
        if (!anythingPressed && Input.anyKey)
        {
                camera.transform.Rotate(angleRotated, 0, 0, Space.Self);
                anythingPressed = true;
                mainMenuButtonCanvas.SetActive(true);
        }
	}

    public void StartGame() //Starts the game at level 1
    {
        SceneManager.LoadScene(startLevel);
    }
    
    public void Settings()
    {
        mainMenuButtonCanvas.SetActive(false);
        settingsCanvas.SetActive(true);
		Debug.Log ("verfjvre");
    }
    

    public void ExitGame() //Closes the application
    {
        Application.Quit();
    }
    
}
/*
    I editor:
    Sett in main camera som camera
    Menu er program prefaben, som bare er den makeshift menyen jeg lagde i paint x) kan skru av denne menyen i prototypen, og kanskje skru av at man tilter kamera?!?!?
    Menyen er ikke synket opp med teaterprogrammet
    MainMenuButtonCanvas er canvaset som holder knappene til main menu (bør være inne automatisk, hvis ikke er det MainMenu canvaset)
    SettingsCanvas er settings canvaset. Bør også ligge inne automatisk
    StartLevel stringen på navngis i editoren. Den må hete NØYAKTIG det som den første scenen (første level) heter!!!! Veldig viktig!!!!!!!!
*/