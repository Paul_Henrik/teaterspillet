﻿using UnityEngine;
using System.Collections;
using System;

public class CameraTarget : MonoBehaviour
{
    //Raycast og target
	public GameObject target;
    public Vector3 rayCastDirection;
    public float switchTargetSpeed = 0.2f;
    public string targetInput;

    //Tilting
    public float timer;                             //Timer on tiltdown
    public bool tiltDown = false;                   //if its watching down
    public Transform lookUp;                        //Gameobject the slerp can assemble to
    public Transform lookDown;                      
    private bool isRunning;                         //If a Enumirator is runnig, it wont run again
    public static bool alreadyUsed = false;         //static so tiltUp is able to run smooth; next tilts turn

    //Delayed Target Follow
    public Vector3 delayedFollowTarget;             //A delayed vector of the position of the target allowing the target to walk without the camera following
    public Vector3 margin;                          //How far the avatar is able to walk.. 20 and higher will get laggy
    public float x;                                 //lerp between targets x and vectors x
    public float z;                                       
    public bool isFollowing;                        //To follow or not to follow.. that is the bool
        


    void start()
    {

    }
    void LateUpdate()
    {
        RaycastTarget();                      //The cameras target    
    }
    public void cameraMovement()                    //The cameras target with delay and margin 
    {
        x = delayedFollowTarget.x;
        z = delayedFollowTarget.z;

        if (isFollowing)
        {
            if (Mathf.Abs(x - target.transform.position.x) > margin.x)
            {
                x = Mathf.Lerp(x, target.transform.position.x, Time.deltaTime);
            }
            if (Mathf.Abs(z - target.transform.position.z) > margin.z)
            {
                z = Mathf.Lerp(z, target.transform.position.z, Time.deltaTime);
            }
            delayedFollowTarget = new Vector3(x, target.transform.position.y, z); 
            transform.LookAt(delayedFollowTarget);
        }
    }

    void Update()
    {
        if (target == null)
        {
            target = GameObject.FindGameObjectWithTag("Player");
            delayedFollowTarget = target.transform.position;
        }
    }

    public void RaycastTarget()
    {
        Vector3 rayCastDirection = target.transform.position - transform.position;          //Distance of the Ray
        Physics.Raycast(transform.position, rayCastDirection);                              //makes the ray
                                                                                            //Debug.DrawRay(transform.position, rayCastDirection, Color.blue);                    //Shows the ray


        if (!alreadyUsed && !isRunning)
           cameraMovement();



    }

    public void LookAtMenu(bool tiltDown)
    {
        if (tiltDown)
        {
            {
                StartCoroutine("coTiltDown");
            }
            
        }
        else
           {
                StartCoroutine("coTiltUp");
           }
    }

    IEnumerator coTiltDown()
    {
        if (isRunning)
            yield break;
        if (alreadyUsed)
            yield break;
        else
            isRunning = true;
        alreadyUsed = true;
        float timer = 0.3f;
        Debug.Log("Before while loop");
        for (int i = 0; i<10; i++)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, lookDown.rotation,  switchTargetSpeed);
            timer -= Time.deltaTime;
            Debug.Log("tilting down");
            yield return new WaitForEndOfFrame();
            
        }
        isRunning = false;
        Debug.Log("isRunning turns false");


    }

    IEnumerator coTiltUp()
    {
        if (isRunning || !alreadyUsed)
            yield break;
        else
            isRunning = true;
        alreadyUsed = false;
        float timer = 0.3f;
        for (int i = 0; i < 10; i++)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, lookUp.rotation,  switchTargetSpeed);
            timer -= Time.deltaTime;
            Debug.Log("tilting up");
            yield return new WaitForEndOfFrame();
        }
        isRunning = false;

    }

}