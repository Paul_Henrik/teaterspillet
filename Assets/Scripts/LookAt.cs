﻿using UnityEngine;
using System.Collections;

public class LookAt : MonoBehaviour
{
    public Vector3 target;
    public bool isPlayer = false;
	// Use this for initialization
	void Start ()
	{

    }

	// Update is called once per frame
	void Update ()
	{
        if (isPlayer) //Finds the position of the delayed vector of the player to avoid unsatisfying lag
            if (CameraTarget.alreadyUsed == true)
            {
                target = GameObject.Find("Camera").GetComponent<CameraTarget>().delayedFollowTarget;
                transform.LookAt(target);
            }
	}
}