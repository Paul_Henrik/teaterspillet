﻿using UnityEngine;
using System.Collections;

public class PrompterIntro : MonoBehaviour
{

    int promptNumber;                       // Keeps track of how many prompts have been seen
    bool promptVisible;                     // Keeps track of a prompt is currently visible
    bool running;                           // Allows spaming of input without jamming the prompts

    // Use this for initialization
    void Start()
    {
        Debug.Log("Awake Prompter");
        promptNumber = 0;
        promptVisible = false;

        // Keeps normal prompts from showing until the intro is over
        this.gameObject.GetComponent<PrompterAI>().introOver = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (promptNumber < 11)
        {

            if (promptVisible == false)
                IntroPrompt();

            // Lets users progress in the intro at their own leisure
            if (promptVisible == true && Input.anyKeyDown)
                IntroPrompt();
        }
    }

    // Makes sure the prompts are shown in the correct order
    void IntroPrompt()
    {
        if (promptNumber == 0)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter(Clone)/Intro/Intro01")));
        else if (promptNumber == 1)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter(Clone)/Intro/Intro02")));
        else if (promptNumber == 2)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter(Clone)/Intro/Intro03")));
        else if (promptNumber == 3)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter(Clone)/Intro/Intro04")));
        else if (promptNumber == 4)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter(Clone)/Intro/Intro05")));
        else if (promptNumber == 5)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter(Clone)/Intro/Intro06")));
        else if (promptNumber == 6)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter(Clone)/Intro/Intro07")));
        else if (promptNumber == 7)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter(Clone)/Intro/Intro08")));
        else if (promptNumber == 8)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter(Clone)/Intro/Intro09")));
        else if (promptNumber == 9)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter(Clone)/Intro/Intro10")));
        else if (promptNumber == 10)
        {
            // Stops the intro and allows normal prompts to start
            this.gameObject.GetComponent<PrompterAI>().introOver = true;
            promptNumber = 11;
            Debug.Log("Intro over");
        }
        else
            Debug.Log("Something went wrong with the prompter in the intro");
    }

    // Visualizes the prompt the introPromt function gives
    IEnumerator VisualizePrompt(GameObject GivenPrompt)
    {
        SpriteRenderer sr = GivenPrompt.GetComponent<SpriteRenderer>();

        // Allows spaming of input without jamming the prompts
        if (!running)
        {
            running = true;

            // Fades in if no prompt is currently visible
            if (promptVisible == false)
            {
                promptVisible = true;
                yield return new WaitForSeconds(0.5f);
                GivenPrompt.gameObject.SetActive(true);

                for (float f = 0f; f <= 1; f += 0.05f)
                {
                    sr.material.color = new Color(f, f, f, f);
                    yield return null;
                }
                sr.material.color = new Color(1f, 1f, 1f, 1f);
                running = false;
                yield break;
            }

            // Fades out if a prompt is visible and player presses any key
            else if (promptVisible == true)
            {
                for (float f = 1f; f >= 0; f -= 0.05f)
                {
                    sr.material.color = new Color(f, f, f, f);
                    yield return null;
                }
                sr.material.color = new Color(0f, 0f, 0f, 0f);
                GivenPrompt.gameObject.SetActive(false);
                promptVisible = false;
                promptNumber += 1;
                running = false;
                yield break;
            }

            // Stops input untill the fade is complete
            else
                yield break;

        }
    }
}
