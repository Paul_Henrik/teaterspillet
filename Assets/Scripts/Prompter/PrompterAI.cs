﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PrompterAI : MonoBehaviour {

    private bool promptVisible;         // Allows for canceling prompt with mouse click without ruining things
    public bool introOver;              // Stops the prompter from giving prompts before the intro is over

    public bool scene1Prompt1;          // Used to communicate with other scripts and visualize correct prompt
    public bool scene1Prompt2;
    public bool scene1Prompt3;

    public bool scene2Prompt1;
    public bool scene2Prompt2;
    public bool scene2Prompt3;

    public bool scene3Prompt1;
    public bool scene3Prompt2;
    public bool scene3Prompt3;

    public bool scene4Prompt1;
    public bool scene4Prompt2;
    public bool scene4Prompt3;

    // Reacts on clicks on the prompter box
    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
            if (introOver == true)
                 SceneCheck();
    }
    
    // Checks current scene
    void SceneCheck ()
    {
        Scene scene = SceneManager.GetActiveScene();

        if (scene.name == "Scene1")
            Scene1Prompt();
        else if (scene.name == "Scene2")
            Scene2Prompt();
        else if (scene.name == "Scene3")
            Scene3Prompt();
        else if (scene.name == "Scene4")
            Scene4Prompt();
        else
            Debug.Log("Scene name is not as given");
    }

    // Gives correct prompt in scene 1
    void Scene1Prompt()
    {
        Debug.Log("Scene1Prompt started");
        if (scene1Prompt1 == false && scene1Prompt2 == false && scene1Prompt3 == false)
            StartCoroutine (VisualizePrompt(GameObject.Find("Prompter/Scene 1/Scene1-OOO")));
        else if (scene1Prompt1 == false && scene1Prompt2 == false && scene1Prompt3 == true)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 1/Scene1-OOX")));
        else if (scene1Prompt1 == false && scene1Prompt2 == true && scene1Prompt3 == false)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 1/Scene1-OXO")));
        else if (scene1Prompt1 == false && scene1Prompt2 == true && scene1Prompt3 == true)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 1/Scene1-OXX")));
        else if (scene1Prompt1 == true && scene1Prompt2 == false && scene1Prompt3 == false)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 1/Scene1-XOO")));
        else if (scene1Prompt1 == true && scene1Prompt2 == false && scene1Prompt3 == true)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 1/Scene1-XOX")));
        else if (scene1Prompt1 == true && scene1Prompt2 == true && scene1Prompt3 == false)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 1/Scene1-XXO")));
        else if (scene1Prompt1 == true && scene1Prompt2 == true && scene1Prompt3 == true)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 1/Scene1-XXX")));
        else
            Debug.Log("Something went wrong with the prompter in scene 1");
    }

    // Gives correct prompt in scene 2
    void Scene2Prompt()
    {
        if (scene2Prompt1 == false && scene2Prompt2 == false && scene2Prompt3 == false)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 2/Scene2-OOO")));
        else if (scene2Prompt1 == false && scene2Prompt2 == false && scene2Prompt3 == true)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 2/Scene2-OOX")));
        else if (scene2Prompt1 == false && scene2Prompt2 == true && scene2Prompt3 == false)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 2/Scene2-OXO")));
        else if (scene2Prompt1 == false && scene2Prompt2 == true && scene2Prompt3 == true)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 2/Scene2-OXX")));
        else if (scene2Prompt1 == true && scene2Prompt2 == false && scene2Prompt3 == false)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 2/Scene2-XOO")));
        else if (scene2Prompt1 == true && scene2Prompt2 == false && scene2Prompt3 == true)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 2/Scene2-XOX")));
        else if (scene2Prompt1 == true && scene2Prompt2 == true && scene2Prompt3 == false)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 2/Scene2-XXO")));
        else if (scene2Prompt1 == true && scene2Prompt2 == true && scene2Prompt3 == true)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 2/Scene2-XXX")));
        else
            Debug.Log("Something went wrong with the prompter in scene 2");
    }

    // Gives correct prompt in scene 3
    void Scene3Prompt()
    {
        if (scene3Prompt1 == false && scene3Prompt2 == false && scene3Prompt3 == false)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 3/Scene3-OOO")));
        else if (scene3Prompt1 == false && scene3Prompt2 == false && scene3Prompt3 == true)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 3/Scene3-OOX")));
        else if (scene3Prompt1 == false && scene3Prompt2 == true && scene3Prompt3 == false)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 3/Scene3-OXO")));
        else if (scene3Prompt1 == false && scene3Prompt2 == true && scene3Prompt3 == true)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 3/Scene3-OXX")));
        else if (scene3Prompt1 == true && scene3Prompt2 == false && scene3Prompt3 == false)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 3/Scene3-XOO")));
        else if (scene3Prompt1 == true && scene3Prompt2 == false && scene3Prompt3 == true)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 3/Scene3-XOX")));
        else if (scene3Prompt1 == true && scene3Prompt2 == true && scene3Prompt3 == false)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 3/Scene3-XXO")));
        else if (scene3Prompt1 == true && scene3Prompt2 == true && scene3Prompt3 == true)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 3/Scene3-XXX")));
        else
            Debug.Log("Something went wrong with the prompter in scene 3");
    }

    // Gives correct prompt in scene 4
    void Scene4Prompt()
    {
        if (scene4Prompt1 == false && scene4Prompt2 == false && scene4Prompt3 == false)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 4/Scene4-OOO")));
        else if (scene4Prompt1 == false && scene4Prompt2 == false && scene4Prompt3 == true)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 4/Scene4-OOX")));
        else if (scene4Prompt1 == false && scene4Prompt2 == true && scene4Prompt3 == false)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 4/Scene4-OXO")));
        else if (scene4Prompt1 == false && scene4Prompt2 == true && scene4Prompt3 == true)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 4/Scene4-OXX")));
        else if (scene4Prompt1 == true && scene4Prompt2 == false && scene4Prompt3 == false)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 4/Scene4-XOO")));
        else if (scene4Prompt1 == true && scene4Prompt2 == false && scene4Prompt3 == true)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 4/Scene4-XOX")));
        else if (scene4Prompt1 == true && scene4Prompt2 == true && scene4Prompt3 == false)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 4/Scene4-XXO")));
        else if (scene4Prompt1 == true && scene4Prompt2 == true && scene4Prompt3 == true)
            StartCoroutine(VisualizePrompt(GameObject.Find("Prompter/Scene 4/Scene4-XXX")));
        else
            Debug.Log("Something went wrong with the prompter in scene 4");
    }

    // Visualizes the prompt the previous SceneXPromt function gives
    IEnumerator VisualizePrompt(GameObject GivenPrompt)
    {
        SpriteRenderer sr = GivenPrompt.GetComponent<SpriteRenderer>();

        // Fades in if no prompt is currently visible
        if (promptVisible == false)
        {
            promptVisible = true;
            GivenPrompt.gameObject.SetActive(true);

                for (float f = 0f; f <= 1; f += 0.05f)
                {
                    sr.material.color = new Color(f, f, f, f);
                    yield return null;
                }
                sr.material.color = new Color(1f, 1f, 1f, 1f);

            // Waits for five seconds, or cancels if player has clicked on prompter box before the time delay is over
            if (promptVisible)
                yield return new WaitForSeconds(0.5f);
            if (promptVisible)
                yield return new WaitForSeconds(0.5f);
            if (promptVisible)
                yield return new WaitForSeconds(0.5f);
            if (promptVisible)
                yield return new WaitForSeconds(0.5f);
            if (promptVisible)
                yield return new WaitForSeconds(0.5f);
            if (promptVisible)
                yield return new WaitForSeconds(0.5f);
            if (promptVisible)
                yield return new WaitForSeconds(0.5f);
            if (promptVisible)
                yield return new WaitForSeconds(0.5f);
            if (promptVisible)
                yield return new WaitForSeconds(0.5f);
            if (promptVisible)
                yield return new WaitForSeconds(0.5f);

            // Fades out if prompt is visible after timed delay
            if (promptVisible)
            {
                for (float f = 1f; f >= 0; f -= 0.05f)
                    {
                        sr.material.color = new Color(f, f, f, f);
                        yield return null;
                    }
                    sr.material.color = new Color(0f, 0f, 0f, 0f);
                }
                GivenPrompt.gameObject.SetActive(false);
                promptVisible = false;
                yield break;
            }

        // Fades out if a prompt is visible and player clicks on prompt box
        else if (promptVisible == true)
        {
                for (float f = 1f; f >= 0; f -= 0.05f)
                {
                    sr.material.color = new Color(f, f, f, f);
                    yield return null;
                }
                sr.material.color = new Color(0f, 0f, 0f, 0f);
            GivenPrompt.gameObject.SetActive(false);
            promptVisible = false;
            yield break;
        }
    }
	void Awake()
	{
		DontDestroyOnLoad (this);
	}
}
