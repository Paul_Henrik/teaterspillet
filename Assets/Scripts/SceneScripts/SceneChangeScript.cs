﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class SceneChangeScript : MonoBehaviour
{

    public Texture2D defaultCursor;
    public Texture2D hoverRight;
    public Texture2D hoverLeft;

    public Vector2 hotSpot = Vector2.zero;
    public CursorMode curMode = CursorMode.Auto;

    public GameObject sceneCarpet;
    public static bool carpetDown;

    public int numberOfScenes;

    public float distanceMultiplier;
    public float sceneTransitionDelay;

    void Start()
    {
        Cursor.SetCursor(defaultCursor, hotSpot, curMode);
    }



    void OnMouseDown()
    {
        if (gameObject.tag == "carpetLeft" && SceneManager.GetActiveScene().buildIndex > 1)
        {
            StartCoroutine(sceneCarpetDownBack());
            Cursor.SetCursor(defaultCursor, hotSpot, curMode);
        }
        if (gameObject.tag == "carpetRight" && SceneManager.GetActiveScene().buildIndex < (numberOfScenes - 1))
        {
            StartCoroutine(sceneCarpetDownForward());
            Cursor.SetCursor(defaultCursor, hotSpot, curMode);
        }
    }

    void OnMouseEnter()
    {
        if (gameObject.tag == "carpetLeft" && SceneManager.GetActiveScene().buildIndex > 0)
        {
            Cursor.SetCursor(hoverLeft, hotSpot, curMode);
        }
        if (gameObject.tag == "carpetRight" && SceneManager.GetActiveScene().buildIndex < (numberOfScenes - 1))
        {
            Cursor.SetCursor(hoverRight, hotSpot, curMode);
        }
    }

    void OnMouseExit()
    {
        Cursor.SetCursor(defaultCursor, hotSpot, curMode);

    }


    IEnumerator sceneCarpetDownBack()
    {
        if (!carpetDown)
        {
            for (int i = 0; i < 4; i++)
            {
                //			sceneCarpet.transform.Translate(Vector3.MoveTowards(transform.position, botPosition.transform.position, distanceMultiplier * 0.5f));
                sceneCarpet.transform.Translate(Vector3.down * distanceMultiplier);
                yield return new WaitForEndOfFrame();

            }
            carpetDown = true;
            yield return new WaitForSeconds(sceneTransitionDelay);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);

            Debug.Log("sieg Heilo");
        }
        else
            yield break;


    }

    IEnumerator sceneCarpetDownForward()
    {
        if (!carpetDown)
        {
            for (int i = 0; i < 4; i++)
            {
                //			sceneCarpet.transform.Translate(Vector3.MoveTowards(transform.position, botPosition.transform.position, distanceMultiplier * 0.5f));
                sceneCarpet.transform.Translate(Vector3.down * distanceMultiplier);
                yield return new WaitForEndOfFrame();
            }
            carpetDown = true;
            yield return new WaitForSeconds(sceneTransitionDelay);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            Debug.Log("sieg Heil");

        }
        else
            yield break;


    }
}


/*
    Set default mouse texture to defaultCursor texture. If PC's default is desired, leave blank.
    Set left arrow texture to hoverLeft texture. This arrow will show when the mouse hovers over the left curtain
    Set right arrow texture to hoverRight texture. This arrow will show when the mouse hovers over the right curtain
    Make sure to set numberOfScenes in the editor so it is easily changeable. Apply this script to both curtains.
    Tag the left curtain "carpetLeft" and the right curtain "carpetRight".
    Make sure the scenes are in the right order in the build settings window. Make sure the mainMenu scene is first in line
    Make sure all scenes that are not playable (scene select etc.) are last in line, and you don't count them when setting the numberOfScenes variable(count mainMenu)
    Call these scenes by name, or adjust their number in the script.
*/

