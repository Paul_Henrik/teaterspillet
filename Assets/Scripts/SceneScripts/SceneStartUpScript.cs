﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneStartUpScript : MonoBehaviour
{

    public GameObject botPosition;
    public GameObject topPosition;

    public GameObject sceneCarpet;

    public bool sceneCarpetIsUp;
    public float maxDistance = 3.5f;


    // Use this for initialization
    void Start()
    {

        sceneCarpetIsUp = false;
        sceneCarpet.transform.position = botPosition.transform.position;

    }

    // Update is called once per frame
    void Update()
    {

        if (!sceneCarpetIsUp)
            StartCoroutine(sceneCarpetUp());

    }

    IEnumerator sceneCarpetUp()
    {
        for (int i = 0; i < 4; i++)
        {
            sceneCarpet.transform.Translate(Vector3.up * maxDistance);//MoveTowards(transform.position, topPosition.transform.position, maxDistance * 0.5f));
            yield return new WaitForEndOfFrame();
        }
        SceneChangeScript.carpetDown = false;
        sceneCarpetIsUp = true;
    }
}
