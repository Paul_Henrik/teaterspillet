﻿using UnityEngine;
using System.Collections;

public class OnMauseClickTest : MonoBehaviour {
    public GameObject InteractionMenu;
    GameObject Player;
    Rigidbody PlayerRigidbody;
    RaycastHit hit;
    Ray ray;

    void Start()
    {
        InteractionMenu.SetActive(false);
    }

    void Update()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.tag != "Interactable" && Input.GetMouseButtonDown(0))
            {
                StartCoroutine(waiting());
            }
        }
			
    }

    IEnumerator waiting()
    {
        yield return new WaitForSeconds(0.2f);
        InteractionMenu.SetActive(false);
    }

    void OnMouseDown()
    {
        InteractionMenu.SetActive(true);
    }
}
