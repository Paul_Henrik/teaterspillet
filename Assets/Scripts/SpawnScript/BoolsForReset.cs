﻿using UnityEngine;
using System.Collections;

public class BoolsForReset : MonoBehaviour
{
    public bool crown;
    public bool sword;
    public bool lute;
    public bool skull;
    public bool shovel;

	public bool showSkull;
	public bool playedLute;
	public bool crownOnKing;
	public bool attackKing;
	public bool attackGuard;
	public bool crownOnGuard;
	public bool hideInBush;
	public bool digUpBush;

	public bool playedSerenade;
	public bool playedMetal2;
	public bool showedSkull2;
	public bool fenceAtJuliet;

    void Start()
    {
		//Items
        crown = false;
        sword = false;
        lute = false;
        skull = false;
        shovel = false;

		//level 1
		showSkull = false;
		playedLute = false;
		crownOnKing = false;
		attackKing = false;
		attackGuard = false;
		crownOnGuard = false;

		//level 2
		playedSerenade = false;
		playedMetal2 = false;
		showedSkull2 = false;
		fenceAtJuliet = false;
		hideInBush = false;
		digUpBush = false;
}
}
