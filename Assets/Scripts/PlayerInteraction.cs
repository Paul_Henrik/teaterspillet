﻿using UnityEngine;
using System.Collections;

public class PlayerInteraction : MonoBehaviour
{
    // Use this for initialization
    public GameObject king;
    public GameObject[] guards;
    PopUp KingEmoji;
    GameObject popUpObject;

    void Start ()
	{
        king = GameObject.FindGameObjectWithTag("King");
        popUpObject = GameObject.Find("KingPopUp");
        KingEmoji = king.GetComponent<PopUp>();
        guards = GameObject.FindGameObjectsWithTag("Guard");
    }

    public void crownSelf()
    {
        GetComponent<BoolsForReset>().crownSelf = true;
        KingEmoji.setEmojiPopup(KingEmoji.angry, true);
        GuardPopUp(KingEmoji.angry, true);
    }

    void GuardPopUp(Material Emoji, bool poppingUp)
    {
        for (int i = 0; i < guards.Length; i++)
        {
            guards[i].GetComponentInChildren<PopUp>().setEmojiPopup(Emoji, poppingUp);
        }
    }
}