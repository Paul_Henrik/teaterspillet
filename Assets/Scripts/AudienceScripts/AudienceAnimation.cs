﻿using UnityEngine;
using System.Collections;

public class AudienceAnimation : MonoBehaviour {

    // Whether or not the audience member is leaving
    public bool Leaving;

    // How fast they get up from the chair
    public float Rise = 0.075f;

    // How much they have gotten up from the chair
    float Risen = 0f;

    // How far they can get up from the chair
	public  float Hold = 2.5f;

    float AlphaFade;

    void Start () {
        // Keeps the audience member from leaving at the start
        Leaving = false;
        AlphaFade = 1f;
    }
	
	void FixedUpdate () {

        // Bool is changed in the AudienceAI script
        if (Leaving == true)
        {
            // Lets the audience member get up from the chair in a calm manner
            if (Risen < Hold)
            {
                Risen = Risen + Rise;
                this.gameObject.transform.Translate(new Vector3(0, Rise, 0));
            }

            // Lets the audience member leave the theatre
            if (Risen >= Hold)
            {
                AlphaFade = Mathf.Clamp01(AlphaFade - (Time.deltaTime / 1f));

                foreach (SpriteRenderer sr in this.GetComponents<SpriteRenderer>())
                    sr.material.color = new Color(AlphaFade, AlphaFade, AlphaFade, AlphaFade);
            }
            if (AlphaFade == 0f)
                this.gameObject.SetActive(false);
        }
    }
}
