﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class AudienceAudio : MonoBehaviour {

    // Connects the script to other scripts and Audio Sources
    public GameObject Audience;
	public GameObject Menu;
    public AudioSource StrongNegativeAudio;
    public AudioSource MildNegativeAudio;
    public AudioSource MildPositiveAudio;
    public AudioSource StrongPositiveAudio;

	Scene scene;

	public GameObject SceneController;
    // Equilent to variables in the AudienceAI script
    bool Reaction;
    int ReactionType;
    int Atmosphere;
    float AudienceVolume;

    //These functions adjust the volume to the current atmosphere and plays the correct clip once
    void StrongNegative(int AtmosphereIn)
    {
        StrongNegativeAudio.volume = (1.0f - (AtmosphereIn * 0.25f)) * AudienceVolume;
        StrongNegativeAudio.Play();
		Debug.Log ("We play 1");
    }

    void MildNegative(int AtmosphereIn)
    {
        MildNegativeAudio.volume = (1.0f - (AtmosphereIn * 0.25f)) * AudienceVolume;
        MildNegativeAudio.Play();
		Debug.Log ("We play 2");
    }

    void MildPositive(int AtmosphereIn)
    {
        MildPositiveAudio.volume = (0.25f + (AtmosphereIn + 0.25f)) * AudienceVolume;
        MildPositiveAudio.Play();
		Debug.Log ("We play 3");
    }

    void StrongPositive(int AtmosphereIn)
    {
        StrongPositiveAudio.volume = (0.25f + (AtmosphereIn + 0.25f)) * AudienceVolume;
        StrongPositiveAudio.Play();
		Debug.Log ("We play 4");
    }

	void OnEnable()
	{
		Menu = GameObject.FindGameObjectWithTag ("SettingsMenu");
//		Menu = GameObject.FindGameObjectWithTag ("PauseMenu");
	}
		

	void Update () {
		scene = SceneManager.GetActiveScene ();
		SceneController = GameObject.FindGameObjectWithTag ("SceneController");
		Menu = GameObject.FindGameObjectWithTag ("PauseMenu").transform.FindChild ("SettingsMenu").gameObject;
		AudienceVolume = Menu.GetComponentInChildren<SettingsMenu>().audience[3].volume;

        // Keeps the variables up to date with their equals in the AudienceAI script
		if (scene.name == "Scene1") {
			Reaction = SceneController.GetComponent<Scene1Script> ().AudienceReacts;
			ReactionType = SceneController.GetComponent<Scene1Script> ().AudienceReaction;
		}
        
		if (scene.name == "Scene2") {
			Reaction = SceneController.GetComponent<scene2Script> ().AudienceReacts;
			ReactionType = SceneController.GetComponent<scene2Script> ().AudienceReaction;
		}

		Atmosphere = Audience.GetComponent<AudienceAI>().Atmosphere;

        // Checks if a clip needs to be played, and then play the correct clip
        if (Reaction == true)
        {
	

            if (ReactionType == 1)
                StrongNegative(Atmosphere);

            else if (ReactionType == 2)
                MildNegative(Atmosphere);

            else if (ReactionType == 3)
                MildPositive(Atmosphere);

            else if (ReactionType == 4)
                StrongPositive(Atmosphere);
        }
    }


}
