﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class AudienceAI : MonoBehaviour {

    // The models in the audience
    public GameObject Male01;
    public GameObject Male02;
    public GameObject Male03;
    public GameObject Male04;
    public GameObject Male05;
    public GameObject Male06;
    public GameObject Female01;
    public GameObject Female02;
    public GameObject Female03;
    public GameObject Female04;
    public GameObject Female05;
    public GameObject Female06;

	Scene scene;

	public GameObject SceneController;

    // How well the audience reacts, on a scale from 1 to 4
	public int ReactionType;

    // Turns true when all of the audience is gone
    public bool GameOver = false;

    // Turns true when an object is validly interacted with
	public bool Reaction;

    // Keeps track on how much the audience currently likes the avatar, on a scale from 0 to 3
    public int Atmosphere = 0;

    // Keeps track on how many there are in the audience
    public int AudienceNumber = 12;

    // Animates the exit of each audience member, put in an aesthetically pleasing order
    void AudienceExit()
    {
        if (AudienceNumber == 1)
            Male02.GetComponent<AudienceAnimation>().Leaving = true;

        else if (AudienceNumber == 2)
            Female05.GetComponent<AudienceAnimation>().Leaving = true;

        else if (AudienceNumber == 3)
            Male04.GetComponent<AudienceAnimation>().Leaving = true;

        else if (AudienceNumber == 4)
            Female01.GetComponent<AudienceAnimation>().Leaving = true;

        else if (AudienceNumber == 5)
            Male06.GetComponent<AudienceAnimation>().Leaving = true;

        else if (AudienceNumber == 6)
            Female02.GetComponent<AudienceAnimation>().Leaving = true;

        else if (AudienceNumber == 7)
            Male05.GetComponent<AudienceAnimation>().Leaving = true;

        else if (AudienceNumber == 8)
            Female03.GetComponent<AudienceAnimation>().Leaving = true;

        else if (AudienceNumber == 9)
            Male03.GetComponent<AudienceAnimation>().Leaving = true;

        else if (AudienceNumber == 10)
            Female04.GetComponent<AudienceAnimation>().Leaving = true;

        else if (AudienceNumber == 11)
            Female06.GetComponent<AudienceAnimation>().Leaving = true;

        else if (AudienceNumber == 12)
            Male01.GetComponent<AudienceAnimation>().Leaving = true;

        else
            Debug.Log("Audience trying to leave, but AudienceNumber is not between 1 and 12");

        // Decreases the number in the audience as they leave
        AudienceNumber--;

        // Checks if all of the audience is gone, and ends the game if they are
        if (AudienceNumber == 0)
            GameOver = true;
    }

	void Awake()
	{
		DontDestroyOnLoad (this.gameObject);
	}

    // Use this for initialization
    void Start () {
    Reaction = false;
    Atmosphere = 0;
    AudienceNumber = 12;


    }
	
	void Update () {
		scene = SceneManager.GetActiveScene ();
		SceneController = GameObject.FindGameObjectWithTag ("SceneController");
		if (scene.name == "Scene1") {
			Reaction = SceneController.GetComponent<Scene1Script> ().AudienceReacts;
			ReactionType = SceneController.GetComponent<Scene1Script> ().AudienceReaction;
		}

		if (scene.name == "Scene2") {
			Reaction = SceneController.GetComponent<scene2Script> ().AudienceReacts;
			ReactionType = SceneController.GetComponent<scene2Script> ().AudienceReaction;
		}

        if (Reaction == true)
        {
            // Adjusts Atmosphere to new reaction
            switch (ReactionType)
            {
                case 1:
                    Atmosphere = Atmosphere - 2;
                    break;
                case 2:
                    Atmosphere = Atmosphere - 1;
                    break;
                case 3:
                    Atmosphere = Atmosphere + 1;
                    break;
                case 4:
                    Atmosphere = Atmosphere + 2;
                    break;
                default:
                    Debug.Log("ReactionType not between 1 and 4");
                    break;
            }

            // Reduces AudienceNumber if Atmosphere is negative
            while (Atmosphere < 0)
            {
                AudienceExit();
				Debug.Log ("Audience leaving");
                Atmosphere = Atmosphere + 1;
            }

            // Sets a max cap of 3 on Atmosphere
            if (Atmosphere > 3)
                Atmosphere = 3;

            // Sets Reaction back to false to stop an infinite loop

        }
    }

	void LateUpdate()
	{
		Reaction = false;
		Debug.Log ("reaction disabled");
	}
}
