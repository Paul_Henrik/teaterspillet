﻿using UnityEngine;
using System.Collections;

public class Trigger : MonoBehaviour {

    // Determines how audience reacts on a scale from 1 to 4
    public int ReactionType;

    // Determines if the audience reacts
    public bool Reaction = false;

    // Implement a check that makes Reaction true if the player has interacted correctly
}
